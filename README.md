# Timebox

A way to keep track of time on various items you're working on throughout the day.

## Installing

`npm install`  
`npx elm-package install`

## Running

`npm start`

Visit `http://localhost:3333/`

## Production build

`npm build`