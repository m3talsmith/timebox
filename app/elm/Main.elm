module Main exposing (main)

import Html exposing (Html, text, div, button)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import TimeboxTask exposing (Task)


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , subscriptions = subs
        , update = update
        , view = view
        }



-- Model


type alias Model =
    { tasks : List Task
    }


init : ( Model, Cmd Msg )
init =
    ( { tasks = []
      }
    , Cmd.none
    )



-- Update


type Msg
    = Add


update : Msg -> Task -> Model -> ( Model, Cmd Msg )
update msg task model =
    case msg of
        Add ->
            ( { model | tasks = task :: model.tasks }, Cmd.none )



-- View


view : Model -> Html Msg
view model =
    div []
        [ div [ class "counter" ]
            [ text (toString model.value) ]
        , div [ class "controls" ]
            [ button [ onClick Increment ] [ text "+1" ]
            , button [ onClick Decrement ] [ text "-1" ]
            ]
        ]



-- Suscriptions


subs : Model -> Sub Msg
subs model =
    Sub.none
