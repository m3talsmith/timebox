module TimeboxTask exposing (..)

import Html exposing (button, div, text, Html)
import Time exposing (Time)


-- Model


type alias Task =
    { title : String
    , estimate : Int
    , running : Bool
    , timePassed : Int
    }


type alias TimeBox =
    { filled : Bool }



-- Init


init : String -> Int -> Task
init title estimate =
    { title = title
    , estimate = estimate
    , running = False
    , timePassed = 0
    }



-- View


render : Task -> Html msg
render task =
    div
        []
        [ text "task rendered" ]
